cross-calc
==========

Academic exercise in making a Java desk calculator

###Installation:

(note: while the calculator is cross-platform the makefile will only work on *nix systems.)

```bash
git clone https://github.com/0x783czar/cross-calc.git
cd cross-calc
make
make install
```


